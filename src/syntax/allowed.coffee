# FSM for lazies

class Allowed
  constructor: (init = {}) -> @[p] = init[p] for p of init
  enableOnly: (what...) ->
    @disableAll()
    @enable(what...)
  enable: (what...) -> @[w] = true for w in what
  disable: (what...) -> @[w] = false for w in what
  disableAll: -> @[w] = false for w of @ when @[w] is true

module.exports = Allowed
