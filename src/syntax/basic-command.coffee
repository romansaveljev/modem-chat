Allowed = require('./allowed')
classify = require('./classify')

module.exports = (command) ->
  result = []
  allowed = new Allowed(
    amp: true, cmd: true, d: true, s: true, sIndex: false,
    sIndexMore: false, equals: false, question: false)
  while command.length > 0
    c = command[0]
    if allowed.d
      if c is 'D'
        # Dial command does not understand =?, dial string follows immediately
        # D is consumed and the rest is expected to be handled by the command itself
        result.push(command.shift())
        break
    if allowed.s
      if c is 'S'
        result.push(command.shift())
        allowed.enableOnly("sIndex")
        continue
    if allowed.amp
      if c is '&'
        result.push(command.shift())
        allowed.enableOnly("cmd")
        continue
    if allowed.cmd
      if classify.isAlpha(c)
        result.push(command.shift())
        allowed.enableOnly("equals", "question")
        continue
      else
        # If the command character is not found where it should be, it is an error
        # otherwise it is hard to avoid endless loops when parsing
        result = []
        break
    if allowed.sIndex
      if classify.isDecimal(c)
        result.push(command.shift())
        allowed.enableOnly("sIndexMore", "equals", "question")
        continue
      else
        # Index shall follow every S command
        result = []
        break
    if allowed.sIndexMore
      if classify.isDecimal(c)
        result.push(command.shift())
        continue
      else
        allowed.enableOnly("equals", "question")
    if allowed.equals
      if c is '='
        result.push(command.shift())
        allowed.enableOnly("question")
        continue
    if allowed.question
      if c is '?'
        result.push(command.shift())
        allowed.disableAll()
        continue
    # Did not meet any criteria - can not continue
    break
  return result
