###
Parses the extended command name after it has been detected, i.e. after +
has been fetched. The goal is to figure out identifiable command and pass it
the rest of the command for consumption. This means certain obvious errors may be
left overlooked at this stage of parsing.
###

Allowed = require('./allowed')
classify = require('./classify')

isExtendedCommandCharacter = (c) ->
  return classify.isAlpha(c) or classify.isDecimal(c) or "!%-./:_".indexOf(c) isnt -1

module.exports = (command) ->
  result = []
  allowed = new Allowed(
    plus: true, alpha: false, extended: false, equals: false,
    question: false)
  while command.length > 0
    c = command[0]
    if allowed.plus
      if c is '+'
        result.push(command.shift())
        allowed.enableOnly("alpha", "extended")
        continue
      else
        throw new Error("BUG! calling extendedCommand() requires a command to start with +")
        break
    if allowed.alpha
      if classify.isAlpha(c)
        result.push(command.shift())
        allowed.enableOnly("extended", "equals", "question")
        continue
      else
        # This is an error, first character is always [A-Z] and single + is not
        # allowed for command
        result = []
        break
    if allowed.extended
      if isExtendedCommandCharacter(c)
        result.push(command.shift())
        continue
      else
        # Try to consume the character again in the next iteration
        allowed.enableOnly("equals", "question")
    if allowed.equals
      if c is '='
        result.push(command.shift())
        allowed.enableOnly("question")
        continue
    if allowed.question
      if c is '?'
        result.push(command.shift())
        allowed.disableAll()
        continue
    # The character did not meet any of criteria - can not continue
    break
  return result
