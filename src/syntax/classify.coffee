isAlphaOne = (c) ->
  "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz".indexOf(c) isnt -1

exports.isAlpha = (c...) -> c.every(isAlphaOne)

isDecimalOne = (c) ->
  "0123456789".indexOf(c) isnt -1

exports.isDecimal = (c...) -> c.every(isDecimalOne)
