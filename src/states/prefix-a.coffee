Echoing = require('./echoing')

class PrefixA extends Echoing
  constructor: (context) ->
    super(context)
    unless @context.v250?.s3?
      throw new Error("v250.s3 must be set to termination character")
  isAttention: (c) ->
    throw new Error("Not implemented - extend and override")
  input: (data) ->
    output = []
    if @isAttention(data[0])
      output = super(data)
      @context.handler = @context.factory.buildAssembleCommand(@context, false)
    else if data[0] is '/'
      output = super(data)
      ###
      ITU-T V.250 5.2.4 Repeating a command line
      > If "A/" is received before any command line has been executed, the
      > preceding command line is assumed to have been empty (that results in
      > an OK result code).
      In practice, it should function as AT<CR>
      ###
      # to keep excessive data handling behavior in one place
      data.unshift(@context.v250.s3)
      data.unshift(@context.repeatable...)
      @context.handler = @context.factory.buildAssembleCommand(@context, true)
    else
      # character is not consumed - could be another A
      @context.handler = @context.factory.buildPrefix(@context)
    return output

module.exports = PrefixA
