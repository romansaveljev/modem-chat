Echoing = require('./echoing')

class AssembleCommand extends Echoing
  constructor: (context, @repeating) ->
    super(context)
    @context.repeatable = []
    unless @context.v250?.s3?
      throw new Error("v250.s3 must be set to termination character")
    unless @context.v250?.s5?
      throw new Error("v250.s5 must be set to editing character")
  onTerminationCharacter: (data) ->
    @context.command = []
    inString = false
    for c in @context.repeatable
      inString = (if c is '"' then not inString else inString)
      if inString
        @context.command.push(c)
      else
        @context.command.push(c.toUpperCase()) if c isnt ' '
    @context.handler = @context.factory.buildExecuteCommand(@context)
    # Trigger it immediately, so it can notify
    @context.handler.input(data)
  onEditingCharacter: (data) ->
    @context.repeatable.pop()
  input: (data) ->
    output = []
    c = data[0]
    if @repeating
      data.shift()
    else
      output = super(data)
    switch c
      when @context.v250.s3
        result = @onTerminationCharacter(data)
        output.push(result...)
      when @context.v250.s5
        @onEditingCharacter(data)
      else
        @context.repeatable.push(c)
    return output

module.exports = AssembleCommand
