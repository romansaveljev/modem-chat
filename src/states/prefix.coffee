Echoing = require('./echoing')

class Prefix extends Echoing
  constructor: (context) ->
    super(context)
  input: (data) ->
    # Typically, either `AT` or `at` are allowed as a command line prefix
    # Neither `At` or `aT` are recognized. So, we need to pass this information
    # to the next state. To override these defaults implement own state factory
    # with custom `buildSmallA()` and `buildCapitalA()` methods
    if data[0] is 'A'
      @context.handler = @context.factory.buildCapitalA(@context)
    else if data[0] is 'a'
      @context.handler = @context.factory.buildSmallA(@context)
    return super(data)

module.exports = Prefix
