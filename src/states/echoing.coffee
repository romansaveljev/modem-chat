ContextHolder = require('./context-holder')

class Echoing extends ContextHolder
  constructor: (context) ->
    super(context)
    unless typeof @context.v250?.echo is 'boolean'
      throw new Error("v250.echo must be boolean")
  input: (data) ->
    c = data.shift()
    if @context.v250.echo
      return [c]
    else
      return []

module.exports = Echoing
