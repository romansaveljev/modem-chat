Prefix = require('./prefix')
PrefixA = require('./prefix-a')
AssembleCommand = require('./assemble-command')
ExecuteCommand = require('./execute-command')

class StateFactory
  buildPrefix: (context) -> new Prefix(context)
  buildCapitalA: (context) ->
    handler = new PrefixA(context)
    handler.isAttention = (c) -> c is 'T'
    return handler
  buildSmallA: (context) ->
    handler = new PrefixA(context)
    handler.isAttention = (c) -> c is 't'
    return handler
  buildAssembleCommand: (context, repeating) ->
    handler = new AssembleCommand(context, repeating)
    oldOnTerminationCharacter = handler.onTerminationCharacter
    handler.onTerminationCharacter = (data) ->
      # By default, it is not allowed to have characters follow the termination
      # character in the command line. The standard is on our side as well:
      # > characters transmitted during the first 125 milliseconds
      # > after transmission of the termination character shall be ignored
      # Good modem driver should wait for response before issuing more commands
      data.splice(0, data.length)
      # fancy and necessary replacement for super
      oldOnTerminationCharacter.call(@, data)
    return handler
  buildExecuteCommand: (context) ->
    handler = new ExecuteCommand(context)
    # TODO: stuff it with default command handlers
    return handler

module.exports = StateFactory
