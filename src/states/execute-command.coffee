ContextHolder = require('./context-holder')
extendedCommand = require('../mixins/extended-command')
basicCommand = require('../mixins/basic-command')
start = require('../mixins/start')
#error = require('../mixins/error')
finalResultCode = require('../mixins/final-result-code')
end = require('../mixins/end')


###
Execute command state handler.

Expects to find current command line from the `@context.command`. It maintains
the active mixin and passes all input data to it. A mixin is a method with a
certain name. This state handler starts at the reserved `start` mixin. Then its
implementation may decide to switch to a different mixin or even to a different
state handler.

Command line is the part between `AT` marker and `S3` termination character (`<CR>`
by default). Neither shall be included to the command line buffer.

@include Error
###
class ExecuteCommand extends ContextHolder
  # It is important to have mixins use goTo
  activeMixin = undefined
  notificationPending = false
  notify = ->
    unless notificationPending
      notificationPending = true
      notifyCb = =>
        notificationPending = false
        @context.notify() if @context.notify
      process.nextTick(notifyCb)
  ###
  @param [Object] context a context object
  ###
  constructor: (context) ->
    super(context)
    @mix(extendedCommand, basicCommand, start, error, end)
    activeMixin = 'start'
  ###
  Extend this state handler with passed mixins.

  Every method from every passed `mixin` becomes a method of this object with the
  same name. Conflicting names are not detected. Last mixin on the list will have
  its methods override everything.

  @param [Object] mixin one or more simple objects with mixin method(s)
  ###
  mix: (mixin...) ->
    for m in mixin
      for p of m
        @[p] = m[p]
  ###
  Activate a different mixin

  @param [String] mixin existing method name to activate
  @throw Mixin name parameter must be a string
  ###
  goTo: (mixin) ->
    unless typeof mixin is "string"
      throw new Error('Mixin must be string')
    unless @[mixin]?
      throw new Error("Mixin #{mixin} must name an existing property")
    unless typeof @[mixin] is "function"
      throw new Error("Mixin #{mixin} must refer to a method")
    activeMixin = mixin
    notify.call(@)
  ###
  Change state handler

  @param [Object] state New state handler
  ###
  setState: (state) ->
    @context.handler = state
    notify.call(@)
  ###
  Reset to a pre-defined initial state

  Modem chat defines an initial state handler and this method will have it jump
  there
  ###
  resetState: -> @setState(undefined)
  ###
  Handle input data or notification

  A state handler may have its `input` method called due to one of two events:
  data received from the input stream, asynchronous notification. The latter may
  be triggered by a mixin or state handler switch.

  Execute command state handler simply passes the data to the active mixin.

  @param [Array] data Input data (may be empty)
  @return [Array] output data to add to the output stream
  ###
  input: (data) ->
    output = @[activeMixin].call(@, data)
    # Every mixin must obey the protocol
    unless output instanceof Array
      throw new Error("Mixin #{activeMixin} must return its output in the array (even empty)")
    return output
  ###
  Format result code

  Caller provides a result text and this method adds header and trailer according
  to the current settings.
  @param [Array] result text to format
  @return [Array] printable output
  ###
  formatResultCode: (result) ->
    output = []
    unless @context.v250.suppressed
      if @context.v250.verbose
        output.push(@context.v250.s3, @context.v250.s4)
      output.push(result...)
      output.push(@context.v250.s3)
      if @context.v250.verbose
        output.push(@context.v250.s4)
    return output

module.exports = ExecuteCommand
