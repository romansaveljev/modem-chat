finalResultCode = (data, verbose, numeric) ->
  @goTo('end')
  result = if @context.v250.verbose then verbose.split('') else numeric.split('')
  return @formatResultCode(result)

###
Basic final result codes as defined in V.250 recommendation.
@mixin
###
FinalResultCodeMixin =
  ###
  `OK` (0) final result code.

  Depending on `V` will output either `OK` or `0` formatted as a final
  result code.
  @param [Array] data any collected input data
  @return [Array] output data to send to the output stream
  ###
  ok: (data) -> finalResultCode.call(@, data, "OK", "0")
  ###
  `NO CARRIER` (3) final result code.

  Depending on `V` will output either `NO CARRIER` or `3` formatted as a final
  result code.
  @param [Array] data any collected input data
  @return [Array] output data to send to the output stream
  ###
  noCarrier: (data) -> finalResultCode.call(@, data, "NO CARRIER", "3")
  ###
  `ERROR` (4) final result code.

  Depending on `V` will output either `ERROR` or `4` formatted as a final
  result code.
  @param [Array] data any collected input data
  @return [Array] output data to send to the output stream
  ###
  error: (data) -> finalResultCode.call(@, data, "ERROR", "4")
  ###
  `NO DIALTONE` (6) final result code.

  Depending on `V` will output either `NO DIALTONE` or `6` formatted as a final
  result code.
  @param [Array] data any collected input data
  @return [Array] output data to send to the output stream
  ###
  noDialtone: (data) -> finalResultCode.call(@, data, "NO DIALTONE", "6")
  ###
  `BUSY` (7) final result code.

  Depending on `V` will output either `BUSY` or `7` formatted as a final
  result code.
  @param [Array] data any collected input data
  @return [Array] output data to send to the output stream
  ###
  busy: (data) -> finalResultCode.call(@, data, "BUSY", "7")
  ###
  `NO ANSWER` (8) final result code.

  Depending on `V` will output either `NO ANSWER` or `8` formatted as a final
  result code.
  @param [Array] data any collected input data
  @return [Array] output data to send to the output stream
  ###
  noAnswer: (data) -> finalResultCode.call(@, data, "NO ANSWER", "8")

module.exports = FinalResultCodeMixin
