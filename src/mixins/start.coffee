module.exports =
  start: (data) ->
    # When the previous command is done, it will jump into start state
    # But then this state will check whether there is anything left to process
    if @context.command.length > 0
      c = @context.command[0]
      # Previous command is responsible of fetching everything up to the point
      # where a next command may start, so the first character should always be
      # meaningful. Implementations may add general separator (;) stripper, if
      # they want to. Here we simply route by the first character to allow for
      # mixing and overrides
      if @[c]?
        @goTo(c)
      else
        @goToError()
    else
      @setState(undefined)
    return []
