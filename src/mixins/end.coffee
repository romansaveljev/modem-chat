dropArray = (arr) -> arr.splice(0, arr.length)

###
Does a final cleanup and prepares modem chat to receive a new command line
@mixin
###
End =
  ###
  Final mixin before the state handler is reset
  @param [Array] data any remaining user input is dropped
  @return [Array] output to send to the output stream
  ###
  end: (data) ->
    dropArray(data)
    dropArray(@context.command)
    @resetState()
    return []

module.exports = End
