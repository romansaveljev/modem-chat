extendedCommand = require('../syntax/extended-command')

module.exports =
  "+": (data) ->
    command = extendedCommand(@context.command)
    if command.length > 0
      name = @[command.join('')]
      if @[name]?
        @goTo(name)
      else
        @goToError()
    else
      @goToError()
    @notify()
    return []
