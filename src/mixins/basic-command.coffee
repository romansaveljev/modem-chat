basicCommand = require('../syntax/basic-command')

extractBasicCommand = (data) ->
  command = basicCommand(@context.command)
  if command.length > 0
    name = " " + command.join('')
    if @[name]?
      @goTo(name)
    else
      @goToError()
  else
    @goToError()
  return []

# Some weird modems will allow commands starting with #
# Try to remain open to possibilities.
# Basic commands are special in that they do not have a prefix. An artificial " "
# will be added in front, so a mixin should declare E command handler as " E".
module.exports =
  A: extractBasicCommand
  B: extractBasicCommand
  C: extractBasicCommand
  D: extractBasicCommand
  E: extractBasicCommand
  F: extractBasicCommand
  G: extractBasicCommand
  H: extractBasicCommand
  I: extractBasicCommand
  J: extractBasicCommand
  K: extractBasicCommand
  L: extractBasicCommand
  M: extractBasicCommand
  N: extractBasicCommand
  O: extractBasicCommand
  P: extractBasicCommand
  Q: extractBasicCommand
  R: extractBasicCommand
  S: extractBasicCommand
  T: extractBasicCommand
  U: extractBasicCommand
  V: extractBasicCommand
  W: extractBasicCommand
  X: extractBasicCommand
  Y: extractBasicCommand
  Z: extractBasicCommand
  "&": extractBasicCommand
