StateFactory = require('./states/state-factory')

class ModemChat
  constructor: (@factory = new StateFactory()) ->
    @v250 =
      echo: true
      s3: "\r"
      s5: "\u0008"
  input: (data) ->
    @handler = @factory.buildPrefix(@) if not @handler
    output = []
    # Run it through the handler at least once, because it could be triggered by
    # notify
    output = @handler.input(data)
    while data.length > 0
      result = @handler.input(data)
      output.push(result...)
    return output

module.exports = ModemChat
