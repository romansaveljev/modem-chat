# modem-chat [![Build Status](https://secure.travis-ci.org/RomanSaveljev/modem-chat.png?branch=master)](https://travis-ci.org/RomanSaveljev/modem-chat)

> AT modem simulation library

### Getting started

`npm install modem-chat`


### Testing 

watches for file changes and reruns tests each time
```bash
  $ grunt watch 
```

runs spec tests
```bash
  $ grunt test  
```

produces coverage report (needs explicit piping)
```bash
  $ grunt cov   
```

## License

MIT
