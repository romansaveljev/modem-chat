extendedCommand = require "#{process.cwd()}/src/syntax/extended-command"

describe 'ExtendedCommand', ->
  it 'should exist', ->
    extendedCommand.should.be.ok
  describe 'happy path', ->
    it 'one character command', ->
      extendedCommand("+A".split('')).should.eql("+A".split(''))
    it 'extracts the only command', ->
      extendedCommand("+ABCD".split('')).should.be.eql("+ABCD".split(''))
    it 'removes command from the buffer', ->
      input = "+ABCD".split('')
      extendedCommand(input)
      input.should.be.empty()
    it 'leaves semicolon', ->
      input = "+ABCD;".split('')
      result = extendedCommand(input)
      result.should.be.eql("+ABCD".split(''))
      input.should.be.eql([";"])
    it 'stops at semicolon', ->
      input = "+ABCD;ZZZ".split('')
      result = extendedCommand(input)
      result.should.be.eql("+ABCD".split(''))
      input.should.be.eql(";ZZZ".split(''))
    it 'detects last read command', ->
      input = "+A?".split('')
      result = extendedCommand(input)
      result.should.be.eql("+A?".split(''))
      input.should.be.empty()
    it 'detects first read command', ->
      input = "+A?;".split('')
      result = extendedCommand(input)
      result.should.be.eql("+A?".split(''))
      input.should.be.eql([";"])
    it 'empty value in parameter command', ->
      input = "+A=".split('')
      result = extendedCommand(input)
      result.should.be.eql("+A=".split(''))
      input.should.be.empty()
    it 'test command', ->
      input = "+ABCD=?".split('')
      result = extendedCommand(input)
      result.should.be.eql("+ABCD=?".split(''))
      input.should.be.empty()
    it 'semicolon after empty parameter command', ->
      input = "+A=;".split('')
      result = extendedCommand(input)
      result.should.be.eql("+A=".split(''))
      input.should.be.eql([";"])
    it 'value in parameter command', ->
      input = "+A=1,22,333".split('')
      result = extendedCommand(input)
      result.should.be.eql("+A=".split(''))
      input.should.be.eql("1,22,333".split(''))
    it 'stops at forbidden character', ->
      # Real modem given "ATE?#" still runs E? and only then throws the error
      input = "+AB&C".split('')
      extendedCommand(input).should.be.eql("+AB".split(''))
      input.should.be.eql("&C".split(''))
    it 'stops at forbidden character in parameter command', ->
      input = "+ABC==".split('')
      extendedCommand(input).should.be.eql("+ABC=".split(''))
      input.should.be.eql(["="])
    it 'stops at forbidden character after read command', ->
      input = "+AB??".split('')
      extendedCommand(input).should.be.eql("+AB?".split(''))
      input.should.be.eql(["?"])
  describe 'error path', ->
    it 'starts with invalid character', ->
      input = "+&".split('')
      extendedCommand(input).should.be.empty()
