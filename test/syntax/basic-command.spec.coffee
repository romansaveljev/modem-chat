basicCommand = require "#{process.cwd()}/src/syntax/basic-command"

describe 'BasicCommand', ->
  it 'should exist', ->
    basicCommand.should.be.ok
  describe 'happy path', ->
    it 'finds simple command', ->
      basicCommand(["A"]).should.be.eql(["A"])
    it 'removes command from the buffer', ->
      input = ["A"]
      basicCommand(input)
      input.should.be.empty()
    it 'finds with ampersand prefix', ->
      input = "&V".split('')
      basicCommand(input).should.be.eql("&V".split(''))
      input.should.be.empty()
    it 'finds D command', ->
      input = ["D"]
      basicCommand(input).should.be.eql(["D"])
      input.should.be.empty()
    it 'finds D with dial string', ->
      input = "D123445#ABD;".split('')
      basicCommand(input).should.be.eql(["D"])
      input.should.be.eql("123445#ABD;".split(''))
    it 'does not handle D?', ->
      input = "D?".split('')
      basicCommand(input).should.be.eql(["D"])
      input.should.be.eql(["?"])
    it 'does not handle D=?', ->
      input = "D=?".split('')
      basicCommand(input).should.be.eql(["D"])
      input.should.be.eql("=?".split(''))
    it 'basic read command', ->
      input = "Z?".split('')
      basicCommand(input).should.be.eql("Z?".split(''))
      input.should.be.empty()
    it 'basic test command', ->
      input = "Y=?".split('')
      basicCommand(input).should.be.eql("Y=?".split(''))
      input.should.be.empty()
    it 'ampersand read command', ->
      input = "&X?".split('')
      basicCommand(input).should.be.eql("&X?".split(''))
      input.should.be.empty()
    it 'ampersand test command', ->
      input = "&W=?".split('')
      basicCommand(input).should.be.eql("&W=?".split(''))
      input.should.be.empty()
    it 'two action commands', ->
      input = "EH".split('')
      basicCommand(input).should.be.eql("E".split(''))
      input.should.be.eql(["H"])
    it 'ampersand command followed by action', ->
      input = "&EG".split('')
      basicCommand(input).should.be.eql("&E".split(''))
      input.should.be.eql(["G"])
    it 'assign a value', ->
      input = "G11".split('')
      basicCommand(input).should.be.eql(["G"])
      input.should.be.eql("11".split(''))
    it 'read command followed by action', ->
      input = "B?&M".split('')
      basicCommand(input).should.be.eql("B?".split(''))
      input.should.be.eql("&M".split(''))
    it 'test command followed by action', ->
      input = "C=?U".split('')
      basicCommand(input).should.be.eql("C=?".split(''))
      input.should.be.eql(["U"])
    it '&D', ->
      input = "&D".split('')
      basicCommand(input).should.be.eql("&D".split(''))
      input.should.be.empty()
    it '&D?', ->
      input = "&D?".split('')
      basicCommand(input).should.be.eql("&D?".split(''))
      input.should.be.empty()
    it '&D=?', ->
      input = "&D=?".split('')
      basicCommand(input).should.be.eql("&D=?".split(''))
      input.should.be.empty()
    it '&D=?', ->
      input = "&D=?".split('')
      basicCommand(input).should.be.eql("&D=?".split(''))
      input.should.be.empty()
    it '&D111', ->
      input = "&D111".split('')
      basicCommand(input).should.be.eql("&D".split(''))
      input.should.be.eql("111".split(''))
    it 'simple S command', ->
      input = "S2=".split('')
      basicCommand(input).should.be.eql("S2=".split(''))
      input.should.be.empty()
    it 'S command with longer index', ->
      input = "S22222=".split('')
      basicCommand(input).should.be.eql("S22222=".split(''))
      input.should.be.empty()
    it 'S read command with longer index', ->
      input = "S123?E".split('')
      basicCommand(input).should.be.eql("S123?".split(''))
      input.should.be.eql(["E"])
    it 'S test command', ->
      input = "S7=?".split('')
      basicCommand(input).should.be.eql("S7=?".split(''))
      input.should.be.empty()
  describe 'error path', ->
    it 'stops if first character is not alpha', ->
      input = ["1"]
      basicCommand(input).should.be.empty()
    it 'double ampersand', ->
      input = "&&".split('')
      basicCommand(input).should.be.empty()
    it 'not alpha after ampersand', ->
      input = "&#".split('')
      basicCommand(input).should.be.empty()
    it 'no index after S', ->
      input = "SS".split('')
      basicCommand(input).should.be.empty()
