exports.toBuffer = (str) -> (str.charCodeAt(i) for i in [0..str.length-1])
