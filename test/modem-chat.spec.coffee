mod = require "#{process.cwd()}/src/modem-chat"

###
describe 'modem-chat', ->
  it 'should exist', ->
    mod.should.be.ok

  describe '#hello', ->
    it 'should return the right value', ->
      mod.hello().should.eql 'hello modem-chat'
###
