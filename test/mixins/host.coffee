class Host
  constructor: (command) ->
    @context =
      command: command.split('')
      v250: {}
    @activeMixin = undefined
    @called = false
    @state = {}
  goTo: (mixin) -> @activeMixin = mixin
  goToStart: -> @activeMixin = 'start'
  goToError: -> @activeMixin = 'ERROR'
  setState: (state) -> @state = state
  mixOne: (mixin) -> @[k] = v for k,v of mixin
  formatResultCode: (result) -> result

module.exports = Host
