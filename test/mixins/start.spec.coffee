start = require "#{process.cwd()}/src/mixins/start"
Host = require('./host')

describe 'Start-mixin', ->
  it 'exists', ->
    start.should.be.ok
  it 'has start method', ->
    start.start.should.be.Function()
  it 'calls method by first character', ->
    host = new Host("C")
    host.C = (data) -> @called = true
    host.mixOne(start)
    host.start([])
    host.activeMixin.should.be.equal('C')
  it 'defaults to ERROR', ->
    host = new Host("C")
    host.mixOne(start)
    host.start([])
    host.activeMixin.should.be.equal('ERROR')
  it 'switches to default state on empty command buffer', ->
    host = new Host("")
    host.mixOne(start)
    host.start([])
    (host.state is undefined).should.be.true()
