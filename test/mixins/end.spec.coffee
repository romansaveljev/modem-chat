###
error = require "#{process.cwd()}/src/mixins/error"
Host = require "./host"

describe 'End-mixin', ->
  it 'exists', ->
    error.should.be.ok
  it 'has error method', ->
    error.error.should.be.Function()
  it 'drops data', ->
    host = new Host("")
    host.mixOne(error)
    data = "abc".split('')
    host.error(data)
    data.should.be.empty()
  it 'drops command buffer', ->
    host = new Host("CDEFGH")
    host.mixOne(error)
    host.error([])
    host.context.command.should.be.empty()
  it 'resets state to undefined', ->
    host = new Host("")
    host.mixOne(error)
    host.error([])
    (host.state is undefined).should.be.true()
###
