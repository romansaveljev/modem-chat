finalResultCode = require "#{process.cwd()}/src/mixins/final-result-code"
Host = require "./host"

generalResultCodeMixinTests = (property, verbose, numeric) ->
  return ->
    it "has #{property} method", ->
      finalResultCode[property].should.be.Function()
    it 'goes to end', ->
      host = new Host("")
      host.context.v250 = verbose: true
      host.mixOne(finalResultCode)
      host[property]([])
      host.activeMixin.should.be.equal("end")
    it "outputs #{verbose} when verbosity is on", ->
      host = new Host("")
      host.mixOne(finalResultCode)
      host.context.v250.verbose = true
      host[property]([]).should.be.eql(verbose.split(''))
    it "outputs #{numeric} when verbosity is off", ->
      host = new Host("")
      host.mixOne(finalResultCode)
      host.context.v250.verbose = false
      host[property]([]).should.be.eql(numeric.split(''))
    it 'outputs what formatResultCode() returns', ->
      expected = "ABCDEFGHz".split('')
      host = new Host("")
      host.formatResultCode = (result) -> expected
      host.mixOne(finalResultCode)
      host.context.v250.verbose = false
      host[property]([]).should.be.eql(expected)

describe 'FinalResultCode-mixin', ->
  it 'exists', ->
    finalResultCode.should.be.ok
  describe 'OK', generalResultCodeMixinTests('ok', 'OK', '0')
  describe 'NO CARRIER', generalResultCodeMixinTests('noCarrier', 'NO CARRIER', '3')
  describe 'ERROR', generalResultCodeMixinTests('error', 'ERROR', '4')
  describe 'NO DIALTONE', generalResultCodeMixinTests('noDialtone', 'NO DIALTONE', '6')
  describe 'BUSY', generalResultCodeMixinTests('busy', 'BUSY', '7')
  describe 'NO ANSWER', generalResultCodeMixinTests('noAnswer', 'NO ANSWER', '8')
