Echoing = require("#{process.cwd()}/src/states/echoing")

describe 'Echoing', ->
  it 'should exist', ->
    Echoing.should.be.ok
  it 'echo enabled by v250.echo', ->
    context =
      v250:
        echo: true
      output: []
    echoing = new Echoing(context)
    data = "ABC".split('')
    output = echoing.input(data)
    output.should.be.eql(["A"])
    data.should.be.eql("BC".split(''))
