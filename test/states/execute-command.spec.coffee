ExecuteCommand = require("#{process.cwd()}/src/states/execute-command")

describe 'ExecuteCommand', ->
  it 'should exist', ->
    ExecuteCommand.should.be.ok
  it 'throws if mixin is not a member', ->
    context =
      command: ["A"]
      notify: () -> exec.input([])
    exec = new ExecuteCommand(context)
    current = ->
      should.fail()
      return []
    (-> exec.setCurrent(current)).should.throw()
    #(-> exec.input(["A"])).should.throw()
  it 'throws if mixin does not return array', ->
    called = false
    context =
      command: ["A"]
      notify: () -> exec.input([])
    exec = new ExecuteCommand(context)
    exec.mixin = ->
      called = true
      return undefined
    exec.setCurrent(exec.mixin)
    (-> exec.input(["A"])).should.throw()
    called.should.be.true()
  it 'correct mixin', ->
    context =
      command: ["A"]
      notify: () -> exec.input([])
    exec = new ExecuteCommand(context)
    exec.mixin = -> return []
    exec.setCurrent(exec.mixin)
    (-> exec.input([])).should.not.throw()
  it 'runs basic command', (done) ->
    context =
      command: ["A"]
      notify: ->
        exec.input([])
    exec = new ExecuteCommand(context)
    exec[" A"] = ->
      done()
      return []
    exec.input([])
  it 'runs extended command', (done) ->
    context =
      command: "+CGMI".split('')
      notify: () -> exec.input([])
    exec = new ExecuteCommand(context)
    exec["+CGMI"] = ->
      done()
      return []
    exec.input([])
  it 'errors on unknown command', (done) ->
    context =
      command: ["#"]
      notify: () -> exec.input([])
    exec = new ExecuteCommand(context)
    exec.ERROR = ->
      done()
      return []
    exec.input([])
  it 'errors on unknown basic command', (done) ->
    context =
      command: "&V".split('')
      notify: () -> exec.input([])
    exec = new ExecuteCommand(context)
    exec.ERROR = ->
      done()
      return []
    exec.input([])
  it 'errors on invalid basic command', (done) ->
    context =
      command: "&&".split('')
      notify: () -> exec.input([])
    exec = new ExecuteCommand(context)
    exec.ERROR = ->
      done()
      return []
    exec[" &&"] = -> should.fail()
    exec.input([])
  it 'errors on unknown extended command', (done) ->
    context =
      command: "+ABRACADABRA".split('')
      notify: () -> exec.input([])
    exec = new ExecuteCommand(context)
    exec.ERROR = ->
      done()
      return []
    exec.input([])
  it 'errors on invalid extended command', (done) ->
    context =
      command: "++G".split('')
      notify: () -> exec.input([])
    exec = new ExecuteCommand(context)
    exec.ERROR = ->
      done()
      return []
    exec["++G"] = ->
      should.fail()
      return []
    exec.input([])
  it 'allows extension beyond basic and extended commands', (done) ->
    context =
      command: "$ARGH".split('')
      notify: () -> exec.input([])
    exec = new ExecuteCommand(context)
    exec['$'] = ->
      done()
      return []
    exec.input([])
  it 'handle multiple basic commands', (done) ->
    calledA = false
    context =
      command: "AB".split('')
      notify: () ->
        exec.input([])
    exec = new ExecuteCommand(context)
    exec[' A'] = ->
      calledA = true
      @notify()
      return []
    exec[' B'] = ->
      calledA.should.be.true()
      done()
      return []
    exec.input([])
  it 'handler receives input data', (done) ->
    expected = [1, 2, 3, 4]
    context =
      command: ["A"]
      notify: () -> exec.input(expected)
    exec = new ExecuteCommand(context)
    exec[' A'] = (data) ->
      data.should.be.eql(expected)
      done()
      return []
    exec.input(expected)
