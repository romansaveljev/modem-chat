# How to write a mixin

Mixin is a contained behavior MIXed INto a state handler. For modem chat mixins
are the additional methods added to command execution state to route a particular
command extracted from the command line to.

## Command line execution

Once a complete and correct command line is collected it will be executed. Example
of a complete command line:

```
ATBCD+E;+F?;G=?H1<CR>
```

The `AT` at the beginning is not handled by execute command state, but included
here for completeness.

Then 3 basic commands follow: `B`, `C` and `D`. A dedicated mixin has to be added
for each of these commands. Command line processing will aborted with error
message, if there is no mixin for a command. Every mixin is responsible of chewing
every next character, which belongs to it. When `B` mixin is complete, the command
buffer should have `C` as its first character, so the next basic command could
be immediately recognized.

By the same rules, `H` basic command must strip `1`.

## Mixin environment

Mixin is added as a method to "execute command" state, i.e. it has access to its
`this` and all methods/fields. Notable items are:

* `this.context` is the same context object available to every state handler; a mixin
may query or modify global variables and even switch to a different handler by
assigning `this.context.handler`
* `this.notify()` allows a mixin to be called asynchronously; a mixin will be
called on user input as well
* `this.goToStart()` switches the state handler into starting state; from the
starting state a new mixin is chosen and executed
* `this.goToError()` should be called by a mixin, when an irrecoverable error is
encountered
* `this.setCurrent()` lets mixin to activate a different mixin; `this.notify()`
is called as well

Example mixin code (CoffeeScript):

```coffeescript
mixin =
  "+ZZ": -> # We want to handle extended command +ZZ
    # Some side effects happen here
    @goToStart() # We are done and let the state machine fetch the next command
```

## Mixin naming

There are certain rules as to how a mixin can be named. When a command name is
fetched, it is resolved to a mixin by locating a property with a specific name.

Modem chat supports two types of commands:

* Basic - starting character belongs to the range `[A-Z&]`
* Extended - starting character is `+`

According to V.250 recommendation, command names are case-insensitive. Note that
we chose to use only upper-case characters.

There are other modems out there, which support e.g. commands starting with `#`.
Additional types of commands is easy to add, but there is a catch to it.

Starting state of command executor will extract the first character from the
command buffer and pick a mixin, which corresponds to this first character. So,
there is in fact a `+` property and a property for every character that can
start a basic command. At that stage mixin's role is to use correct method to
extract a full command name and activate it. To add support for custom commands
starting with `#` our state handler needs to have a `#` property.

Basic command `A` mixin has to be assigned to `A` property, but it is already
assigned and extracts the basic command itself. Some basic commands are more than
one character and there is a dedicated parser for it. It is possible to replace
default basic command `A` parsing mixin by assigning `A` property. In this case
custom implementation needs to take care of recognizing and routing also `A?` and
`A=?`. Default implementation handles them as individual basic commands. When
parsing mixin `A` finds that the command is neither read nor test command, then
it will cal ` A` property. All spaces except inside string constants are stripped
from the command line, so this artificial leading character helps us to keep the
implementation simple.

Upper-case property names are reserved for command mixins. Other methods may use
mixed-case or lower-case notation.

## Mixin implementation

Mixin is a method that handles a command from the command line. Mixins are packaged
into objects, where a handling function is referenced by a property named as a
command it handles. Mixins may be written in any language that transpiles to
JavaScript:

```javascript
mixin = {
  " A=?": function(data) {
    // data must be consumed, otherwise it will be offered over and over again
    this.dropArray(data)
    // `A` basic command is supported and does not have any parameters, no need
    // to print anything, because the start state will generate OK/ERROR response
    // for the whole command line by itself
    this.goToStart();
    // Every mixin must return an array (even empty) to represent the output,
    // which is sent to the screen
    return []
  },
  " A?": function(data) {
    // `A` is not a parameter command, so reading it is an error
    // Here we could generate a special error message
    this.goToError();
    return []
  }
  // We could add the same error logic for `A=` command, but simply skipping it
  // will generate an error
  "+CSCS=": function(data) {
    // The command buffer begins with what follows `=` in `+CSCS=`. It might be
    // another command or end of buffer or command separator. The mixin is responsible
    // of sorting it out.
    // Here the mixin must consume parameters from this.context.command,
    // before jumping to the starting state. The command buffer must be in such
    // a shape that start state could pick up a next command from it
    return []
  }
};

// Mixin is added to our execute command state handler
// All its properties are merged with executor properties
// Mixin takes precedence and overrides all existing properties
executor.mix(mixin)
```

Things to note:

* `return []` - every mixin must return some output to render for the user; it
very well may be an empty array; an exception is thrown unless a mixin complies
* Active mixin is given control over the input stream; current chunk of input
data is passed as an array; it may be an empty array; mixin should consume the
data and update it in-place with `data.shift()`; mixin will be called repeatedly
until the `data` is empty (but at least once)

## Finishing a mixin

There is four ways how a mixin may complete and deactivate. Regardless of a
chosen path, mixin must always return an array with output (even empty).

### Abort on error

If an error condition is detected in the command line, a mixin may elect to abort
further command line execution with an error. Error handler is a mixin by itself,
i.e. it can be overriden.

Default error handler prints error message according to settings drops all user
input and remaining command buffer and activates result code mixin.

Aborting is realized through `this.goToError()` call. The method will send
notification by itself.

The above description produces a general `ERROR` result code. Own mixins exist
for the rest of the result codes. Depending on their nature they can jump to
either `finalResultCode` or `intermediateResultCode`.

### Return to starting state

Starting state will try to parse next command from the remaining command buffer.
It is very important to leave a command buffer in such a state, where parsing
next command is easily possible. Basic commands do not need any separator between
them, while an extended command must be followed by a separator unless it is
the last on the command line. The separator must be chopped off by a mixin as well.

Returning to a starting state happens, when `this.goToStart()` is invoked. It will
asynchronously notify as well, so the starting state will be activated immediately.

When the starting state is entered with empty command buffer, it produces `OK`
result code immediately. For instance, handling of `AT<CR>` effortlessly results
in `OK` response. So, once a mixin pushed out all its output and emptied the
command buffer, it should still jump to the starting state.

### Activate a custom mixin

Instead of returning to the starting state, a mixin may activate a different one.
Call `this.goTo(mixin)` to switch and notify. The `mixin` parameter is a string
name of the property.

### Change the state

A mixin could switch to a different state handler by calling `this.setState()`
with a different object. Notification will be sent out automatically. An `undefined`
value plays a special role by forcing the modem chat to enter the first state
(whatever it is defined).
