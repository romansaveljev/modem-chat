# Can the project be written in Groovy?

Groovy is an optionally dynamic scripting/compiling language, which can cover
for CoffeeScript shortcomings.

It compiles to JVM byte code and enjoys all power of Java and all its libraries
along with a proper building system. Very little architecture is should need to
be reconsidered.

## Architectural stepping stones

CoffeeScript (and NodeJS) has been chosen over few of its features that are
necessary for application functioning.

### Safe asynchronous event handling

NodeJS with its single-threading environment relieves its users of all the pain
related with asynchronous IO. To implement it the same we will use
[Executors#newSingleThreadExecutor](https://docs.oracle.com/javase/7/docs/api/java/util/concurrent/Executors.html#newSingleThreadExecutor()).

State handler gets called through a single end-point `input`. It may happen, when
either some data becomes available or a notification has been triggered.

In CoffeeScript implementation notification callback might bring some input data
(or none) to `input`. Input data had higher priority. This is not really important
from an actual modem perspective. The behavior is undefined. Nowhere is specified
what is happening to the input data, when internal workings of the modem take place.
It might be left in an input buffer, or it might be discarded or it might be fetched
for the processing. If a certain sub-state wants some input data, they should
calmly wait for it.

STDIN reading should be running in its own thread, but the data will be processed
in order with notifications. Catching an input data will cause an anonymous `Runnable`
object posted to the executor's queue. Different kind of `Runnable` will be posted
on notification. Single thread executor will thus implement a cooperative scheduling.

### State handlers hierarchy

Groovy with its "optional" static typization can greatly enhance the architecture.
Static types will be enforced to reduce the cognitive load. API violations will
become compile time errors. State handlers will be declared as interface. State
factory may use `Expando` for a quick ad-hoc extension or instantiate anonymous
abstract class objects.

### Strict where it must be

Parts of the system may be written in a conventional Java (for whatever reason).

### Method overloading

This is possible with CoffeeScript as well, but works natively with Groovy. Overloading
may enhance mixin API, where we could explicitly state we accept maps, single
key-value pairs or anonymous interface implementations (for calling from Java).

### Mixins

Mixins may enjoy `Expando` class capabilities. User will add new mixins by assigning
closures to new/existing properties. A state handler object is available, so a
mixin may call it directly. In this case a closure should be generated for every
state handler object, because it needs access to the object itself.

Another option is to override `invokeMethod` and to keep mixins in a map of closures.
Every mixin will be called through `with` method, i.e. `this` will refer to the
state handler.

Neither of these options will allow for
