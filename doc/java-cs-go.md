Java or CofeeScript or Go?

Java pros:

* Can use proper IDE, debugger, logging and testing
* Can use maven
* The project begins to grow class hierarchies - static typing is necessary
* Support for more platforms (e.g. Android)
* Project is used as a library, so the language needs to be familiar and
documentable

Java cons:

* Has to be compiled and packaged
* Single-threaded execution model is hard to get right, user creates a complex
interface
* Hard to mix command handlers into a command executor
* Hard to have extensible context - type has to be declared
* A lot of boring code

CofeeScript pros:

* Eloquent minimalist code
* Dynamic typing does not stop from doing tricks
* Scripted - distribute as a source code
* Asynchronous I/O with node execution model prevents dead-locks and other
nasty synchronization issues
* Users will write many small test modems, which will expose certain behaviors,
i.e. have to be easy on users
* Documentable with [codo](https://github.com/coffeedoc/codo) or
[coffeedoc](https://github.com/omarkhan/coffeedoc)
* Does not have to write user applications in CofeeScript
* Testable and good development workflow with grunt
* yeoman can generate a node application skeleton

CofeeScript cons:

* Syntax is not familiar
* Logging and debugging is a challenge - have to rely on unit tests
* Runtime errors are hard to figure out
* Syntax errors are cryptic

Go pros:

* Can be distributed as source code and executed with `go run`
* Many errors are caught at compilation time
* Package management

Go cons:

* Unfamiliar syntax
* Premature debugger
* Need some framework to create the data flow
* Supports Android
* Overriding and inheritance is hard
